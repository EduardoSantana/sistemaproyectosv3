<?php

namespace App\Repositories;

use App\Models\ObrasPresupuestos;
use InfyOm\Generator\Common\BaseRepository;

class ObrasPresupuestosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'obra_objeto_id',
        'presupuesto_tipo_id',
        'presupuesto_etapa_id',
        'codigo',
        'unidad',
        'descripcion',
        'precio',
        'cantidad',
        'Valor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ObrasPresupuestos::class;
    }
}
