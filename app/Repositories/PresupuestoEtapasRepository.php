<?php

namespace App\Repositories;

use App\Models\PresupuestoEtapas;
use InfyOm\Generator\Common\BaseRepository;

class PresupuestoEtapasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PresupuestoEtapas::class;
    }
}
