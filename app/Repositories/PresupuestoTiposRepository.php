<?php

namespace App\Repositories;

use App\Models\PresupuestoTipos;
use InfyOm\Generator\Common\BaseRepository;

class PresupuestoTiposRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PresupuestoTipos::class;
    }
}
