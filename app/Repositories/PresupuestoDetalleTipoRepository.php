<?php

namespace App\Repositories;

use App\Models\PresupuestoDetalleTipo;
use InfyOm\Generator\Common\BaseRepository;

class PresupuestoDetalleTipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PresupuestoDetalleTipo::class;
    }
}
