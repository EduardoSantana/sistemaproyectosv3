<?php

namespace App\Repositories;

use App\Models\PresupuestoDetalles;
use InfyOm\Generator\Common\BaseRepository;

class PresupuestoDetallesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'obras_presupuestos_id',
        'presupuesto_detalle_tipos_id',
        'codigo',
        'unidad',
        'descripcion',
        'precio',
        'cantidad',
        'Valor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PresupuestoDetalles::class;
    }
}
