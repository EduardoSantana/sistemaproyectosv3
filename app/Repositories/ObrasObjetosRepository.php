<?php

namespace App\Repositories;

use App\Models\ObrasObjetos;
use InfyOm\Generator\Common\BaseRepository;

class ObrasObjetosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'obra_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ObrasObjetos::class;
    }
}
