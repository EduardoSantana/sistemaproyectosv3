<?php

namespace App\Repositories;

use App\Models\ObrasEtapas;
use InfyOm\Generator\Common\BaseRepository;

class ObrasEtapasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ObrasEtapas::class;
    }
}
