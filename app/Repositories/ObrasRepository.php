<?php

namespace App\Repositories;

use App\Models\Obras;
use InfyOm\Generator\Common\BaseRepository;

class ObrasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'obra_etapa_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Obras::class;
    }
}
