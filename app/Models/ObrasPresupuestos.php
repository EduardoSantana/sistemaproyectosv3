<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ObrasPresupuestos",
 *      required={"obra_objeto_id", "presupuesto_tipo_id", "presupuesto_etapa_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="obra_objeto_id",
 *          description="obra_objeto_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="presupuesto_tipo_id",
 *          description="presupuesto_tipo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="presupuesto_etapa_id",
 *          description="presupuesto_etapa_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="codigo",
 *          description="codigo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="categoria",
 *          description="categoria",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unidad",
 *          description="unidad",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="precio",
 *          description="precio",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="cantidad",
 *          description="cantidad",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="Valor",
 *          description="Valor",
 *          type="number",
 *          format="float"
 *      )
 * )
 */
class ObrasPresupuestos extends Model
{
    use SoftDeletes;

    public $table = 'obras_presupuestos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'obra_objeto_id',
        'presupuesto_tipo_id',
        'presupuesto_etapa_id',
        'codigo',
        'categoria',
        'unidad',
        'descripcion',
        'precio',
        'cantidad',
        'Valor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'obra_objeto_id' => 'integer',
        'presupuesto_tipo_id' => 'integer',
        'presupuesto_etapa_id' => 'integer',
        'codigo' => 'string',
        'categoria' => 'string',
        'unidad' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'obra_objeto_id' => 'required',
        'presupuesto_tipo_id' => 'required',
        'presupuesto_etapa_id' => 'required'
    ];

    public function obraObjeto()
    {
        return $this->belongsTo(ObrasObjetos::class, 'obra_objeto_id');
    }
    public function presupuestoTipo()
    {
        return $this->belongsTo(PresupuestoTipos::class, 'presupuesto_tipo_id');
    }
    public function presupuestoEtapa()
    {
        return $this->belongsTo(PresupuestoEtapas::class, 'presupuesto_etapa_id');
    }


}
