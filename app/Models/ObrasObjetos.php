<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ObrasObjetos",
 *      required={"nombre", "obra_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="obra_id",
 *          description="obra_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class ObrasObjetos extends Model
{
    use SoftDeletes;

    public $table = 'obras_objetos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'descripcion',
        'obra_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'obra_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'obra_id' => 'required'
    ];

    public function obra()
    {
        return $this->belongsTo(Obras::class, 'obra_id');
    }

    public function obrasPresupuesto()
    {
        return $this->hasMany(ObrasPresupuesto::class, 'obra_objeto_id');
    }

}
