<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="PresupuestoDetalles",
 *      required={"obras_presupuestos_id", "presupuesto_detalle_tipos_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="obras_presupuestos_id",
 *          description="obras_presupuestos_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="presupuesto_detalle_tipos_id",
 *          description="presupuesto_detalle_tipos_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="codigo",
 *          description="codigo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="categoria",
 *          description="categoria",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unidad",
 *          description="unidad",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="precio",
 *          description="precio",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="cantidad",
 *          description="cantidad",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="Valor",
 *          description="Valor",
 *          type="number",
 *          format="float"
 *      )
 * )
 */
class PresupuestoDetalles extends Model
{
    use SoftDeletes;

    public $table = 'presupuesto_detalles';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'obras_presupuestos_id',
        'presupuesto_detalle_tipos_id',
        'codigo',
        'categoria',
        'unidad',
        'descripcion',
        'precio',
        'cantidad',
        'Valor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'obras_presupuestos_id' => 'integer',
        'presupuesto_detalle_tipos_id' => 'integer',
        'codigo' => 'string',
        'categoria' => 'string',
        'unidad' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'obras_presupuestos_id' => 'required',
        'presupuesto_detalle_tipos_id' => 'required'
    ];
}
