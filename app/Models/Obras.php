<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Obras",
 *      required={"nombre"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="obra_etapa_id",
 *          description="obra_etapa_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Obras extends Model
{
    use SoftDeletes;

    public $table = 'obras';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'descripcion',
        'obra_etapa_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'obra_etapa_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required'
    ];

    public function obrasEtapa()
    {
        return $this->belongsTo(ObrasEtapas::class, 'obra_etapa_id');
    }

    public function obrasObjetos()
    {
        return $this->hasMany(ObrasObjetos::class, 'obra_id');
    }
}
