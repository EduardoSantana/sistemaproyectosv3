<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePresupuestoDetallesRequest;
use App\Http\Requests\UpdatePresupuestoDetallesRequest;
use App\Repositories\PresupuestoDetallesRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PresupuestoDetallesController extends InfyOmBaseController
{
    /** @var  PresupuestoDetallesRepository */
    private $presupuestoDetallesRepository;

    public function __construct(PresupuestoDetallesRepository $presupuestoDetallesRepo)
    {
        $this->middleware('auth');
        $this->presupuestoDetallesRepository = $presupuestoDetallesRepo;
    }

    /**
     * Display a listing of the PresupuestoDetalles.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->presupuestoDetallesRepository->pushCriteria(new RequestCriteria($request));
        $presupuestoDetalles = $this->presupuestoDetallesRepository->all();

        return view('presupuestoDetalles.index')
            ->with('presupuestoDetalles', $presupuestoDetalles);
    }

    /**
     * Show the form for creating a new PresupuestoDetalles.
     *
     * @return Response
     */
    public function create()
    {
        return view('presupuestoDetalles.create');
    }

    /**
     * Store a newly created PresupuestoDetalles in storage.
     *
     * @param CreatePresupuestoDetallesRequest $request
     *
     * @return Response
     */
    public function store(CreatePresupuestoDetallesRequest $request)
    {
        $input = $request->all();

        $presupuestoDetalles = $this->presupuestoDetallesRepository->create($input);

        Flash::success('PresupuestoDetalles saved successfully.');

        return redirect(route('presupuestoDetalles.index'));
    }

    /**
     * Display the specified PresupuestoDetalles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presupuestoDetalles = $this->presupuestoDetallesRepository->findWithoutFail($id);

        if (empty($presupuestoDetalles)) {
            Flash::error('PresupuestoDetalles not found');

            return redirect(route('presupuestoDetalles.index'));
        }

        return view('presupuestoDetalles.show')->with('presupuestoDetalles', $presupuestoDetalles);
    }

    /**
     * Show the form for editing the specified PresupuestoDetalles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presupuestoDetalles = $this->presupuestoDetallesRepository->findWithoutFail($id);

        if (empty($presupuestoDetalles)) {
            Flash::error('PresupuestoDetalles not found');

            return redirect(route('presupuestoDetalles.index'));
        }

        return view('presupuestoDetalles.edit')->with('presupuestoDetalles', $presupuestoDetalles);
    }

    /**
     * Update the specified PresupuestoDetalles in storage.
     *
     * @param  int              $id
     * @param UpdatePresupuestoDetallesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresupuestoDetallesRequest $request)
    {
        $presupuestoDetalles = $this->presupuestoDetallesRepository->findWithoutFail($id);

        if (empty($presupuestoDetalles)) {
            Flash::error('PresupuestoDetalles not found');

            return redirect(route('presupuestoDetalles.index'));
        }

        $presupuestoDetalles = $this->presupuestoDetallesRepository->update($request->all(), $id);

        Flash::success('PresupuestoDetalles updated successfully.');

        return redirect(route('presupuestoDetalles.index'));
    }

    /**
     * Remove the specified PresupuestoDetalles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presupuestoDetalles = $this->presupuestoDetallesRepository->findWithoutFail($id);

        if (empty($presupuestoDetalles)) {
            Flash::error('PresupuestoDetalles not found');

            return redirect(route('presupuestoDetalles.index'));
        }

        $this->presupuestoDetallesRepository->delete($id);

        Flash::success('PresupuestoDetalles deleted successfully.');

        return redirect(route('presupuestoDetalles.index'));
    }
}
