<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePresupuestoEtapasRequest;
use App\Http\Requests\UpdatePresupuestoEtapasRequest;
use App\Repositories\PresupuestoEtapasRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PresupuestoEtapasController extends InfyOmBaseController
{
    /** @var  PresupuestoEtapasRepository */
    private $presupuestoEtapasRepository;

    public function __construct(PresupuestoEtapasRepository $presupuestoEtapasRepo)
    {
        $this->middleware('auth');
        $this->presupuestoEtapasRepository = $presupuestoEtapasRepo;
    }

    /**
     * Display a listing of the PresupuestoEtapas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->presupuestoEtapasRepository->pushCriteria(new RequestCriteria($request));
        $presupuestoEtapas = $this->presupuestoEtapasRepository->all();

        return view('presupuestoEtapas.index')
            ->with('presupuestoEtapas', $presupuestoEtapas);
    }

    /**
     * Show the form for creating a new PresupuestoEtapas.
     *
     * @return Response
     */
    public function create()
    {
        return view('presupuestoEtapas.create');
    }

    /**
     * Store a newly created PresupuestoEtapas in storage.
     *
     * @param CreatePresupuestoEtapasRequest $request
     *
     * @return Response
     */
    public function store(CreatePresupuestoEtapasRequest $request)
    {
        $input = $request->all();

        $presupuestoEtapas = $this->presupuestoEtapasRepository->create($input);

        Flash::success('PresupuestoEtapas saved successfully.');

        return redirect(route('presupuestoEtapas.index'));
    }

    /**
     * Display the specified PresupuestoEtapas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presupuestoEtapas = $this->presupuestoEtapasRepository->findWithoutFail($id);

        if (empty($presupuestoEtapas)) {
            Flash::error('PresupuestoEtapas not found');

            return redirect(route('presupuestoEtapas.index'));
        }

        return view('presupuestoEtapas.show')->with('presupuestoEtapas', $presupuestoEtapas);
    }

    /**
     * Show the form for editing the specified PresupuestoEtapas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presupuestoEtapas = $this->presupuestoEtapasRepository->findWithoutFail($id);

        if (empty($presupuestoEtapas)) {
            Flash::error('PresupuestoEtapas not found');

            return redirect(route('presupuestoEtapas.index'));
        }

        return view('presupuestoEtapas.edit')->with('presupuestoEtapas', $presupuestoEtapas);
    }

    /**
     * Update the specified PresupuestoEtapas in storage.
     *
     * @param  int              $id
     * @param UpdatePresupuestoEtapasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresupuestoEtapasRequest $request)
    {
        $presupuestoEtapas = $this->presupuestoEtapasRepository->findWithoutFail($id);

        if (empty($presupuestoEtapas)) {
            Flash::error('PresupuestoEtapas not found');

            return redirect(route('presupuestoEtapas.index'));
        }

        $presupuestoEtapas = $this->presupuestoEtapasRepository->update($request->all(), $id);

        Flash::success('PresupuestoEtapas updated successfully.');

        return redirect(route('presupuestoEtapas.index'));
    }

    /**
     * Remove the specified PresupuestoEtapas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presupuestoEtapas = $this->presupuestoEtapasRepository->findWithoutFail($id);

        if (empty($presupuestoEtapas)) {
            Flash::error('PresupuestoEtapas not found');

            return redirect(route('presupuestoEtapas.index'));
        }

        $this->presupuestoEtapasRepository->delete($id);

        Flash::success('PresupuestoEtapas deleted successfully.');

        return redirect(route('presupuestoEtapas.index'));
    }
}
