<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use App\Http\Requests\CreateObrasObjetosRequest;
use App\Http\Requests\UpdateObrasObjetosRequest;
use App\Repositories\ObrasObjetosRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ObrasObjetosController extends InfyOmBaseController
{
    /** @var  ObrasObjetosRepository */
    private $obrasObjetosRepository;

    public function __construct(ObrasObjetosRepository $obrasObjetosRepo)
    {
        $this->middleware('auth');
        $this->obrasObjetosRepository = $obrasObjetosRepo;
    }

    /**
     * Display a listing of the ObrasObjetos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->obrasObjetosRepository->pushCriteria(new RequestCriteria($request));
        $obrasObjetos = $this->obrasObjetosRepository->all();

        return view('obrasObjetos.index')
            ->with('obrasObjetos', $obrasObjetos);
    }

    /**
     * Show the form for creating a new ObrasObjetos.
     *
     * @return Response
     */
    public function create()
    {
        $dropDownObra = DB::table('obras')->get();
        return view('obrasObjetos.create')->with('dropDownObra',$dropDownObra);
    }

    /**
     * Store a newly created ObrasObjetos in storage.
     *
     * @param CreateObrasObjetosRequest $request
     *
     * @return Response
     */
    public function store(CreateObrasObjetosRequest $request)
    {
        $input = $request->all();

        $obrasObjetos = $this->obrasObjetosRepository->create($input);

        Flash::success('ObrasObjetos saved successfully.');

        return redirect(route('obrasObjetos.index'));
    }

    /**
     * Display the specified ObrasObjetos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obrasObjetos = $this->obrasObjetosRepository->findWithoutFail($id);

        if (empty($obrasObjetos)) {
            Flash::error('ObrasObjetos not found');

            return redirect(route('obrasObjetos.index'));
        }

        return view('obrasObjetos.show')->with('obrasObjetos', $obrasObjetos);
    }

    /**
     * Show the form for editing the specified ObrasObjetos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
              
        $obrasObjetos = $this->obrasObjetosRepository->findWithoutFail($id);

        if (empty($obrasObjetos)) {
            Flash::error('ObrasObjetos not found');

            return redirect(route('obrasObjetos.index'));
        }
        $dropDownObra = DB::table('obras')->get();
        return view('obrasObjetos.edit')->with('obrasObjetos', $obrasObjetos)->with('dropDownObra',$dropDownObra);
    }

    /**
     * Update the specified ObrasObjetos in storage.
     *
     * @param  int              $id
     * @param UpdateObrasObjetosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObrasObjetosRequest $request)
    {
        $obrasObjetos = $this->obrasObjetosRepository->findWithoutFail($id);

        if (empty($obrasObjetos)) {
            Flash::error('ObrasObjetos not found');

            return redirect(route('obrasObjetos.index'));
        }

        $obrasObjetos = $this->obrasObjetosRepository->update($request->all(), $id);

        Flash::success('ObrasObjetos updated successfully.');

        return redirect(route('obrasObjetos.index'));
    }

    /**
     * Remove the specified ObrasObjetos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $obrasObjetos = $this->obrasObjetosRepository->findWithoutFail($id);

        if (empty($obrasObjetos)) {
            Flash::error('ObrasObjetos not found');

            return redirect(route('obrasObjetos.index'));
        }

        $this->obrasObjetosRepository->delete($id);

        Flash::success('ObrasObjetos deleted successfully.');

        return redirect(route('obrasObjetos.index'));
    }
}
