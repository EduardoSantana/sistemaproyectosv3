<?php

namespace App\Http\Controllers;

use View;
use DB;
use App\Http\Requests;
use App\Http\Requests\CreateObrasRequest;
use App\Http\Requests\UpdateObrasRequest;
use App\Repositories\ObrasRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ObrasController extends InfyOmBaseController
{
    /** @var  ObrasRepository */
    private $obrasRepository;

    public function __construct(ObrasRepository $obrasRepo)
    {
        $this->middleware('auth');
        $this->obrasRepository = $obrasRepo;
    }

    /**
     * Display a listing of the Obras.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $masterPage = 'layouts.app';
        $esAjax = $request->ajax();
        if($esAjax == true){
            $masterPage = 'layouts.popup';
        }
        
        $this->obrasRepository->pushCriteria(new RequestCriteria($request));
        $obras = $this->obrasRepository->all();
        
        
        return view('obras.index')
            ->with('obras', $obras)
            ->with('masterPage', $masterPage);
    }

    /**
     * Show the form for creating a new Obras.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $masterPage = 'layouts.app';
        $esAjax = $request->ajax();
        if($esAjax == true){
            $masterPage = 'layouts.popup';
        }
        $dropDownObraEtapa = DB::table('obras_etapas')->get();
        return view('obras.create')->with('dropDownObraEtapa',$dropDownObraEtapa)->with('masterPage', $masterPage);
    }

    /**
     * Store a newly created Obras in storage.
     *
     * @param CreateObrasRequest $request
     *
     * @return Response
     */
    public function store(CreateObrasRequest $request)
    {
        $input = $request->all();

        $obras = $this->obrasRepository->create($input);

        Flash::success('Obras saved successfully.');

        return redirect(route('obras.index'));
    }

    /**
     * Display the specified Obras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obras = $this->obrasRepository->findWithoutFail($id);

        if (empty($obras)) {
            Flash::error('Obras not found');

            return redirect(route('obras.index'));
        }

        return view('obras.show')->with('obras', $obras);
    }

    /**
     * Show the form for editing the specified Obras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $obras = $this->obrasRepository->findWithoutFail($id);

        if (empty($obras)) {
            Flash::error('Obras not found');

            return redirect(route('obras.index'));
        }
        $dropDownObraEtapa = DB::table('obras_etapas')->get();
        return view('obras.edit')->with('obras', $obras)->with('dropDownObraEtapa',$dropDownObraEtapa);
    }

    /**
     * Update the specified Obras in storage.
     *
     * @param  int              $id
     * @param UpdateObrasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObrasRequest $request)
    {
        $obras = $this->obrasRepository->findWithoutFail($id);

        if (empty($obras)) {
            Flash::error('Obras not found');

            return redirect(route('obras.index'));
        }

        $obras = $this->obrasRepository->update($request->all(), $id);

        Flash::success('Obras updated successfully.');

        return redirect(route('obras.index'));
    }

    /**
     * Remove the specified Obras from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $obras = $this->obrasRepository->findWithoutFail($id);

        if (empty($obras)) {
            Flash::error('Obras not found');

            return redirect(route('obras.index'));
        }

        $this->obrasRepository->delete($id);

        Flash::success('Obras deleted successfully.');

        return redirect(route('obras.index'));
    }
}
