<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateObrasEtapasRequest;
use App\Http\Requests\UpdateObrasEtapasRequest;
use App\Repositories\ObrasEtapasRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ObrasEtapasController extends InfyOmBaseController
{
    /** @var  ObrasEtapasRepository */
    private $obrasEtapasRepository;

    public function __construct(ObrasEtapasRepository $obrasEtapasRepo)
    {
        $this->middleware('auth');
        $this->obrasEtapasRepository = $obrasEtapasRepo;
    }

    /**
     * Display a listing of the ObrasEtapas.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->obrasEtapasRepository->pushCriteria(new RequestCriteria($request));
        $obrasEtapas = $this->obrasEtapasRepository->all();

        return view('obrasEtapas.index')
            ->with('obrasEtapas', $obrasEtapas);
    }

    /**
     * Show the form for creating a new ObrasEtapas.
     *
     * @return Response
     */
     public function create(Request $request)
    {
        $masterPage = 'layouts.app';
        $esAjax = $request->ajax();
        if($esAjax == true){
            $masterPage = 'layouts.popup';
        }
        return view('obrasEtapas.create')->with('masterPage', $masterPage);
    }

    /**
     * Store a newly created ObrasEtapas in storage.
     *
     * @param CreateObrasEtapasRequest $request
     *
     * @return Response
     */
    public function store(CreateObrasEtapasRequest $request)
    {
        $input = $request->all();

        $obrasEtapas = $this->obrasEtapasRepository->create($input);

        Flash::success('ObrasEtapas saved successfully.');

        return redirect(route('obrasEtapas.index'));
    }

    /**
     * Display the specified ObrasEtapas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obrasEtapas = $this->obrasEtapasRepository->findWithoutFail($id);

        if (empty($obrasEtapas)) {
            Flash::error('ObrasEtapas not found');

            return redirect(route('obrasEtapas.index'));
        }

        return view('obrasEtapas.show')->with('obrasEtapas', $obrasEtapas);
    }

    /**
     * Show the form for editing the specified ObrasEtapas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $obrasEtapas = $this->obrasEtapasRepository->findWithoutFail($id);

        if (empty($obrasEtapas)) {
            Flash::error('ObrasEtapas not found');

            return redirect(route('obrasEtapas.index'));
        }

        return view('obrasEtapas.edit')->with('obrasEtapas', $obrasEtapas);
    }

    /**
     * Update the specified ObrasEtapas in storage.
     *
     * @param  int              $id
     * @param UpdateObrasEtapasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObrasEtapasRequest $request)
    {
        $obrasEtapas = $this->obrasEtapasRepository->findWithoutFail($id);

        if (empty($obrasEtapas)) {
            Flash::error('ObrasEtapas not found');

            return redirect(route('obrasEtapas.index'));
        }

        $obrasEtapas = $this->obrasEtapasRepository->update($request->all(), $id);

        Flash::success('ObrasEtapas updated successfully.');

        return redirect(route('obrasEtapas.index'));
    }

    /**
     * Remove the specified ObrasEtapas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $obrasEtapas = $this->obrasEtapasRepository->findWithoutFail($id);

        if (empty($obrasEtapas)) {
            Flash::error('ObrasEtapas not found');

            return redirect(route('obrasEtapas.index'));
        }

        $this->obrasEtapasRepository->delete($id);

        Flash::success('ObrasEtapas deleted successfully.');

        return redirect(route('obrasEtapas.index'));
    }
}
