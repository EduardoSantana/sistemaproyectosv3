<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePresupuestoDetalleTipoRequest;
use App\Http\Requests\UpdatePresupuestoDetalleTipoRequest;
use App\Repositories\PresupuestoDetalleTipoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PresupuestoDetalleTipoController extends InfyOmBaseController
{
    /** @var  PresupuestoDetalleTipoRepository */
    private $presupuestoDetalleTipoRepository;

    public function __construct(PresupuestoDetalleTipoRepository $presupuestoDetalleTipoRepo)
    {
        $this->middleware('auth');
        $this->presupuestoDetalleTipoRepository = $presupuestoDetalleTipoRepo;
    }

    /**
     * Display a listing of the PresupuestoDetalleTipo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->presupuestoDetalleTipoRepository->pushCriteria(new RequestCriteria($request));
        $presupuestoDetalleTipos = $this->presupuestoDetalleTipoRepository->all();

        return view('presupuestoDetalleTipos.index')
            ->with('presupuestoDetalleTipos', $presupuestoDetalleTipos);
    }

    /**
     * Show the form for creating a new PresupuestoDetalleTipo.
     *
     * @return Response
     */
    public function create()
    {
        return view('presupuestoDetalleTipos.create');
    }

    /**
     * Store a newly created PresupuestoDetalleTipo in storage.
     *
     * @param CreatePresupuestoDetalleTipoRequest $request
     *
     * @return Response
     */
    public function store(CreatePresupuestoDetalleTipoRequest $request)
    {
        $input = $request->all();

        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->create($input);

        Flash::success('PresupuestoDetalleTipo saved successfully.');

        return redirect(route('presupuestoDetalleTipos.index'));
    }

    /**
     * Display the specified PresupuestoDetalleTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->findWithoutFail($id);

        if (empty($presupuestoDetalleTipo)) {
            Flash::error('PresupuestoDetalleTipo not found');

            return redirect(route('presupuestoDetalleTipos.index'));
        }

        return view('presupuestoDetalleTipos.show')->with('presupuestoDetalleTipo', $presupuestoDetalleTipo);
    }

    /**
     * Show the form for editing the specified PresupuestoDetalleTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->findWithoutFail($id);

        if (empty($presupuestoDetalleTipo)) {
            Flash::error('PresupuestoDetalleTipo not found');

            return redirect(route('presupuestoDetalleTipos.index'));
        }

        return view('presupuestoDetalleTipos.edit')->with('presupuestoDetalleTipo', $presupuestoDetalleTipo);
    }

    /**
     * Update the specified PresupuestoDetalleTipo in storage.
     *
     * @param  int              $id
     * @param UpdatePresupuestoDetalleTipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresupuestoDetalleTipoRequest $request)
    {
        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->findWithoutFail($id);

        if (empty($presupuestoDetalleTipo)) {
            Flash::error('PresupuestoDetalleTipo not found');

            return redirect(route('presupuestoDetalleTipos.index'));
        }

        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->update($request->all(), $id);

        Flash::success('PresupuestoDetalleTipo updated successfully.');

        return redirect(route('presupuestoDetalleTipos.index'));
    }

    /**
     * Remove the specified PresupuestoDetalleTipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presupuestoDetalleTipo = $this->presupuestoDetalleTipoRepository->findWithoutFail($id);

        if (empty($presupuestoDetalleTipo)) {
            Flash::error('PresupuestoDetalleTipo not found');

            return redirect(route('presupuestoDetalleTipos.index'));
        }

        $this->presupuestoDetalleTipoRepository->delete($id);

        Flash::success('PresupuestoDetalleTipo deleted successfully.');

        return redirect(route('presupuestoDetalleTipos.index'));
    }
}
