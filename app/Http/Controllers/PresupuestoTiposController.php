<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePresupuestoTiposRequest;
use App\Http\Requests\UpdatePresupuestoTiposRequest;
use App\Repositories\PresupuestoTiposRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PresupuestoTiposController extends InfyOmBaseController
{
    /** @var  PresupuestoTiposRepository */
    private $presupuestoTiposRepository;

    public function __construct(PresupuestoTiposRepository $presupuestoTiposRepo)
    {
        $this->middleware('auth');
        $this->presupuestoTiposRepository = $presupuestoTiposRepo;
    }

    /**
     * Display a listing of the PresupuestoTipos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->presupuestoTiposRepository->pushCriteria(new RequestCriteria($request));
        $presupuestoTipos = $this->presupuestoTiposRepository->all();

        return view('presupuestoTipos.index')
            ->with('presupuestoTipos', $presupuestoTipos);
    }

    /**
     * Show the form for creating a new PresupuestoTipos.
     *
     * @return Response
     */
    public function create()
    {
        return view('presupuestoTipos.create');
    }

    /**
     * Store a newly created PresupuestoTipos in storage.
     *
     * @param CreatePresupuestoTiposRequest $request
     *
     * @return Response
     */
    public function store(CreatePresupuestoTiposRequest $request)
    {
        $input = $request->all();

        $presupuestoTipos = $this->presupuestoTiposRepository->create($input);

        Flash::success('PresupuestoTipos saved successfully.');

        return redirect(route('presupuestoTipos.index'));
    }

    /**
     * Display the specified PresupuestoTipos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $presupuestoTipos = $this->presupuestoTiposRepository->findWithoutFail($id);

        if (empty($presupuestoTipos)) {
            Flash::error('PresupuestoTipos not found');

            return redirect(route('presupuestoTipos.index'));
        }

        return view('presupuestoTipos.show')->with('presupuestoTipos', $presupuestoTipos);
    }

    /**
     * Show the form for editing the specified PresupuestoTipos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $presupuestoTipos = $this->presupuestoTiposRepository->findWithoutFail($id);

        if (empty($presupuestoTipos)) {
            Flash::error('PresupuestoTipos not found');

            return redirect(route('presupuestoTipos.index'));
        }

        return view('presupuestoTipos.edit')->with('presupuestoTipos', $presupuestoTipos);
    }

    /**
     * Update the specified PresupuestoTipos in storage.
     *
     * @param  int              $id
     * @param UpdatePresupuestoTiposRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePresupuestoTiposRequest $request)
    {
        $presupuestoTipos = $this->presupuestoTiposRepository->findWithoutFail($id);

        if (empty($presupuestoTipos)) {
            Flash::error('PresupuestoTipos not found');

            return redirect(route('presupuestoTipos.index'));
        }

        $presupuestoTipos = $this->presupuestoTiposRepository->update($request->all(), $id);

        Flash::success('PresupuestoTipos updated successfully.');

        return redirect(route('presupuestoTipos.index'));
    }

    /**
     * Remove the specified PresupuestoTipos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $presupuestoTipos = $this->presupuestoTiposRepository->findWithoutFail($id);

        if (empty($presupuestoTipos)) {
            Flash::error('PresupuestoTipos not found');

            return redirect(route('presupuestoTipos.index'));
        }

        $this->presupuestoTiposRepository->delete($id);

        Flash::success('PresupuestoTipos deleted successfully.');

        return redirect(route('presupuestoTipos.index'));
    }
}
