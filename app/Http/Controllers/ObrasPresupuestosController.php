<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateObrasPresupuestosRequest;
use App\Http\Requests\UpdateObrasPresupuestosRequest;
use App\Repositories\ObrasPresupuestosRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ObrasPresupuestosController extends InfyOmBaseController
{
    /** @var  ObrasPresupuestosRepository */
    private $obrasPresupuestosRepository;

    public function __construct(ObrasPresupuestosRepository $obrasPresupuestosRepo)
    {
        $this->middleware('auth');
        $this->obrasPresupuestosRepository = $obrasPresupuestosRepo;
    }

    /**
     * Display a listing of the ObrasPresupuestos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->obrasPresupuestosRepository->pushCriteria(new RequestCriteria($request));
        $obrasPresupuestos = $this->obrasPresupuestosRepository->all();

        return view('obrasPresupuestos.index')
            ->with('obrasPresupuestos', $obrasPresupuestos);
    }

    /**
     * Show the form for creating a new ObrasPresupuestos.
     *
     * @return Response
     */
    public function create()
    {
        return view('obrasPresupuestos.create');
    }

    /**
     * Store a newly created ObrasPresupuestos in storage.
     *
     * @param CreateObrasPresupuestosRequest $request
     *
     * @return Response
     */
    public function store(CreateObrasPresupuestosRequest $request)
    {
        $input = $request->all();

        $obrasPresupuestos = $this->obrasPresupuestosRepository->create($input);

        Flash::success('ObrasPresupuestos saved successfully.');

        return redirect(route('obrasPresupuestos.index'));
    }

    /**
     * Display the specified ObrasPresupuestos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obrasPresupuestos = $this->obrasPresupuestosRepository->findWithoutFail($id);

        if (empty($obrasPresupuestos)) {
            Flash::error('ObrasPresupuestos not found');

            return redirect(route('obrasPresupuestos.index'));
        }

        return view('obrasPresupuestos.show')->with('obrasPresupuestos', $obrasPresupuestos);
    }

    /**
     * Show the form for editing the specified ObrasPresupuestos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $obrasPresupuestos = $this->obrasPresupuestosRepository->findWithoutFail($id);

        if (empty($obrasPresupuestos)) {
            Flash::error('ObrasPresupuestos not found');

            return redirect(route('obrasPresupuestos.index'));
        }

        return view('obrasPresupuestos.edit')->with('obrasPresupuestos', $obrasPresupuestos);
    }

    /**
     * Update the specified ObrasPresupuestos in storage.
     *
     * @param  int              $id
     * @param UpdateObrasPresupuestosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObrasPresupuestosRequest $request)
    {
        $obrasPresupuestos = $this->obrasPresupuestosRepository->findWithoutFail($id);

        if (empty($obrasPresupuestos)) {
            Flash::error('ObrasPresupuestos not found');

            return redirect(route('obrasPresupuestos.index'));
        }

        $obrasPresupuestos = $this->obrasPresupuestosRepository->update($request->all(), $id);

        Flash::success('ObrasPresupuestos updated successfully.');

        return redirect(route('obrasPresupuestos.index'));
    }

    /**
     * Remove the specified ObrasPresupuestos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $obrasPresupuestos = $this->obrasPresupuestosRepository->findWithoutFail($id);

        if (empty($obrasPresupuestos)) {
            Flash::error('ObrasPresupuestos not found');

            return redirect(route('obrasPresupuestos.index'));
        }

        $this->obrasPresupuestosRepository->delete($id);

        Flash::success('ObrasPresupuestos deleted successfully.');

        return redirect(route('obrasPresupuestos.index'));
    }
}
