<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\ObrasPresupuestos;
use App\Models\PresupuestoTipos;
use App\Models\PresupuestoDetalles;
use App\Models\PresupuestoEtapas;
use App\Http\Requests\CreateObrasRequest;
use App\Http\Requests\UpdateObrasRequest;
use App\Repositories\ObrasRepository;
use App\Repositories\ObrasObjetosRepository;
use App\Repositories\PresupuestoTiposRepository;
use App\Repositories\ObrasPresupuestosRepository;
use App\Repositories\PresupuestoEtapasRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProyectosController extends InfyOmBaseController
{
    /** @var  ObrasRepository */
    private $obrasRepository;
    private $obrasObjetosRepository;
    private $presupuestoTiposRepository;
    private $obrasPresupuestosRepository;
    private $presupuestoEtapasRepository;

    public function __construct(ObrasRepository $obrasRepo, ObrasObjetosRepository $obrasObjetosRepo, PresupuestoTiposRepository $presupuestoTiposRepo, ObrasPresupuestosRepository $obrasPresupuestosRepo, PresupuestoEtapasRepository $presupuestoEtapasRepo)
    {
        $this->middleware('auth');
        $this->obrasRepository = $obrasRepo;
        $this->obrasObjetosRepository = $obrasObjetosRepo;
        $this->presupuestoTiposRepository = $presupuestoTiposRepo;
        $this->obrasPresupuestosRepository = $obrasPresupuestosRepo;
        $this->presupuestoEtapasRepository = $presupuestoEtapasRepo;
    }

    /**
     * Display a listing of the Obras.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->obrasRepository->pushCriteria(new RequestCriteria($request));
        $obras = $this->obrasRepository->all();

        return view('proyectos.index')
            ->with('obras', $obras);
    }

    /**
     * Display the specified Obras.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obras = $this->obrasRepository->findWithoutFail($id);

        if (empty($obras)) {
            Flash::error('Proyectos not found');
            return redirect(route('obras.index'));
        }
        
        $objetosObra = $obras->obrasObjetos;
        
        $presupuestoTipo = PresupuestoTipos::get();
        
        return view('proyectos.show')
            ->with('obras', $obras)
            ->with('presupuestoTipo', $presupuestoTipo)
            ->with('objetosObra', $objetosObra);
    }

    public function objetosPrespuestos($objeto_id, $tipo_id, $etapa_id = 0)
    {
        $dropDownTipoPresupuesto = $this->presupuestoTiposRepository->all();
        $dropDownEtapas = $this->presupuestoEtapasRepository->all();
        $obrasObjeto = $this->obrasObjetosRepository->findWithoutFail($objeto_id);
        $presupuestoTipo = $this->presupuestoTiposRepository->findWithoutFail($tipo_id);
        
        //dd($presupuestoTipo);
        
        if (empty($obrasObjeto)) {
            Flash::error('Obra Objetos not found');
            return redirect(route('proyectos.index'));
        }

        if (empty($presupuestoTipo)) {
            Flash::error('Presupuesto Tipo not found');
            return redirect(route('proyectos.index'));
        }
        
        return view('proyectos.objetosPrespuestos')
            ->with('obrasObjeto', $obrasObjeto)
            ->with('presupuestoTipo', $presupuestoTipo)
            ->with('dropDownTipoPresupuesto', $dropDownTipoPresupuesto)
            ->with('dropDownEtapas', $dropDownEtapas)
            ->with('etapa_id', $etapa_id)
            ->with('tipo_id', $tipo_id)
            ->with('objeto_id', $objeto_id);
    }
    
    public function presupuestoDetalle(Request $request, $id)
    {
        $masterPage = 'layouts.app';
        $esAjax = $request->ajax();
        if($esAjax == true){
            $masterPage = 'layouts.popup';
        }
       
        $obraPresupuesto = ObrasPresupuestos::find($id);
        
        return view('proyectos.presupuestoDetalle')
            ->with('masterPage', $masterPage)
            ->with('obraPresupuesto', $obraPresupuesto)
            ->with('presupuesto_id', $id);
    }
    
    public function etapaPro()
    {
        
        $data = PresupuestoEtapas::select('nombre as DisplayText','id as Value')->get();
        return Response::json(
            array(
                "Result"			=>		"OK",
                "Options"			=>		$data
            )
        );
            		
    }
    
    public function tipoPro()
    {
        
        $data = PresupuestoTipos::select('nombre as DisplayText','id as Value')->get();
        return Response::json(
            array(
                "Result"			=>		"OK",
                "Options"			=>		$data
            )
        );
            		
    }
    
    public function getData($action)
    {
       
        $mainTable = 'obras_presupuestos';
        $objeto_id = Request("objeto_id");
        $tipo_id = Request("tipo_id");
        $etapa_id = Request("etapa_id");
        $jtSorting = Request('jtSorting');
        $jtStartIndex = Request('jtStartIndex');
        $jtPageSize = Request('jtPageSize');
       	
        if(false){
             //prueba de variables recibida
            $testArray = array(
                "mainTable" 	=>	$mainTable,
                "objeto_id" 	=>	$objeto_id,
                "tipo_id" 	=>	$tipo_id,
                "etapa_id" 	=>	$etapa_id,
                "jtSorting" 	=>	$jtSorting,
                "jtStartIndex" 	=>	$jtStartIndex,
                "jtPageSize" 	=>	$jtPageSize
            );
        
            dd($testArray);
        }
        
    	switch ($action) 
    	{
    		case 'list':
              
                if($etapa_id > 0){
    				    $rows = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                            ['presupuesto_etapa_id',$etapa_id],
                        ])->count();
    			}else{
        				$rows = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                        ])->count();
    			}
                
                if (Request('jtSorting')) 
    			{
    				$search = explode(" ", Request("jtSorting"));
    				if($etapa_id > 0){
    				    $data = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                            ['presupuesto_etapa_id',$etapa_id],
                        ])
                        ->skip(Request("jtStartIndex"))
        				->take(Request("jtPageSize"))
        				->orderBy($search[0], $search[1])
        				->get();
    				}else{
        				$data = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                        ])
                        ->skip(Request("jtStartIndex"))
        				->take(Request("jtPageSize"))
        				->orderBy($search[0], $search[1])
        				->get();
    				}
    			
    			}
    			else
    			{
    			    if($etapa_id > 0){
    				    $data = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                            ['presupuesto_etapa_id',$etapa_id],
                        ])
                        ->skip(Request("jtStartIndex"))
        				->take(Request("jtPageSize"))
        				->get();
    				}else{
        				$data = ObrasPresupuestos::where([
                            ['obra_objeto_id',$objeto_id],
                            ['presupuesto_tipo_id',$tipo_id],
                        ])
                        ->skip(Request("jtStartIndex"))
        				->take(Request("jtPageSize"))
        				->get();
    				}
    			    
    			}
    			
    			return Response::json(
    				array(
    					"Result"			=>		"OK",
    					"TotalRecordCount"	=>		$rows,
    					"Records"			=>		$data,
    				)
    			);
    			
    		break;
    			
    		case 'create':

        		$item = new ObrasPresupuestos();
                $item->obra_objeto_id = Request("objeto_id");
                $item->presupuesto_tipo_id = Request("presupuesto_tipo_id");
                $item->presupuesto_etapa_id = Request("presupuesto_etapa_id");
                $item->codigo = Request("codigo");
                $item->unidad = Request("unidad");
                $item->descripcion = Request("descripcion");
                $item->precio = Request("precio");
                $item->cantidad = Request("cantidad");
                $item->Valor = $item->precio * $item->cantidad;
    			
    			if($item->save())
    			{
    				$toView = ObrasPresupuestos::find($item->id);
    				
    				return Response::json(array(
    						"Result"			=>		"OK",
    						"Record"			=>		$toView
    					)
    				);
    			}
    			
    		break;
    			
    		case 'update':

                $tempPrecio = Request("precio");
                $item = ObrasPresupuestos::find(Request("id"));
                
                if($tempPrecio < 0){
                    //$collection->sum('pages');
                    // $item->Valor = $item->precio * $item->cantidad;
                }else{
                    
                    $item->presupuesto_etapa_id = Request("presupuesto_etapa_id");
                    $item->codigo = Request("codigo");
                    $item->unidad = Request("unidad");
                    $item->descripcion = Request("descripcion");
                    $item->precio = Request("precio");
                    $item->cantidad = Request("cantidad");
                    $item->Valor = $item->precio * $item->cantidad;
                }
                
                if($item->save())
                {
                    $toView = ObrasPresupuestos::find($item->id);
                    return Response::json(array(
    						"Result"			=>		"OK",
    						"Record"			=>		$toView
    					)
    				);
                }
                
    		break;
    			
    		case 'delete':
    		    
     			$item = ObrasPresupuestos::find(Request("id"));
     			
     			if($item->delete())
     			{
     				return Response::json(array(
     						"Result"			=>		"OK",
     					)
     				);
     			}
     			
    		break;
    		
    	}
    }
    
    public function getDataDetalle($action)
    {
        $presupuesto_id = Request("presupuesto_id");
        $presupuesto_detalle_tipos_id = Request("presupuesto_detalle_tipos_id");
        $jtSorting = Request('jtSorting');
        $jtStartIndex = Request('jtStartIndex');
        $jtPageSize = Request('jtPageSize');
       	
        if(false){
            $testArray = array(
                "jtSorting" 	=>	$jtSorting,
                "jtStartIndex" 	=>	$jtStartIndex,
                "jtPageSize" 	=>	$jtPageSize
            );
        
            dd($testArray);
        }
        
    	switch ($action) 
    	{
    		case 'list':
    		    
    		    $rows = PresupuestoDetalles::where([
                    ['obras_presupuestos_id',$presupuesto_id],
                    ['presupuesto_detalle_tipos_id',$presupuesto_detalle_tipos_id],
                ])->count();
                
                if (Request('jtSorting')) 
    			{
    				$search = explode(" ", Request("jtSorting"));
    			    $data = PresupuestoDetalles::where([
                        ['obras_presupuestos_id',$presupuesto_id],
                        ['presupuesto_detalle_tipos_id',$presupuesto_detalle_tipos_id],
                    ])
                    ->skip(Request("jtStartIndex"))
        			->take(Request("jtPageSize"))
        			->orderBy($search[0], $search[1])
        			->get();
    			}
    			else
    			{
    				$data = PresupuestoDetalles::where([
                        ['obras_presupuestos_id',$presupuesto_id],
                        ['presupuesto_detalle_tipos_id',$presupuesto_detalle_tipos_id],
                    ])
                    ->skip(Request("jtStartIndex"))
        			->take(Request("jtPageSize"))
        			->get();

    			}
    			
    			return Response::json(
    				array(
    					"Result"			=>		"OK",
    					"TotalRecordCount"	=>		$rows,
    					"Records"			=>		$data,
    				)
    			);
    			
    		break;
    			
    		case 'create':

        		$item = new PresupuestoDetalles();
                $item->obras_presupuestos_id = Request("presupuesto_id");
                $item->presupuesto_detalle_tipos_id = Request("presupuesto_detalle_tipos_id");
                $item->codigo = Request("codigo");
                $item->unidad = Request("unidad");
                $item->descripcion = Request("descripcion");
                $item->precio = Request("precio");
                $item->cantidad = Request("cantidad");
                //$item->Valor = Request("Valor");
                $item->Valor = $item->precio * $item->cantidad;
    			
    			if($item->save())
    			{
    			    $toView = PresupuestoDetalles::find($item->id);
    			    $collection = PresupuestoDetalles::where('obras_presupuestos_id',$toView->obras_presupuestos_id);
    			    $itemPadre = ObrasPresupuestos::find($toView->obras_presupuestos_id);
    			    $itemPadre->precio = $collection->sum('Valor');
    			    $itemPadre->Valor = $itemPadre->precio * $itemPadre->cantidad;
    			    
    			    if(!$itemPadre->save()){
    			        return Response::json(array(
    						"Result"			=>		"ERROR"
        					)
        				);
    			    }
    			    
    				
    				
    				return Response::json(array(
    						"Result"			=>		"OK",
    						"Record"			=>		$toView
    					)
    				);
    			}
    			
    		break;
    			
    		case 'update':
                
                $item = PresupuestoDetalles::find(Request("id"));
                $item->codigo = Request("codigo");
                $item->unidad = Request("unidad");
                $item->descripcion = Request("descripcion");
                $item->precio = Request("precio");
                $item->cantidad = Request("cantidad");
                //$item->Valor = Request("Valor");
                $item->Valor = $item->precio * $item->cantidad;
                
                if($item->save())
                {
                    $toView = PresupuestoDetalles::find($item->id);
    			    $collection = PresupuestoDetalles::where('obras_presupuestos_id',$toView->obras_presupuestos_id);
    			    $itemPadre = ObrasPresupuestos::find($toView->obras_presupuestos_id);
    			    $itemPadre->precio = $collection->sum('Valor');
    			    $itemPadre->Valor = $itemPadre->precio * $itemPadre->cantidad;
    			    
    			    if(!$itemPadre->save()){
    			        return Response::json(array(
    						"Result"			=>		"ERROR"
        					)
        				);
    			    }
    			    
                    return Response::json(array(
    						"Result"			=>		"OK",
    						"Record"			=>		$toView
    					)
    				);

                }
                
    		break;
    			
    		case 'delete':
    		    
     			$item = PresupuestoDetalles::find(Request("id"));
     			$collection = PresupuestoDetalles::where('obras_presupuestos_id',$item->obras_presupuestos_id);
    			$itemPadre = ObrasPresupuestos::find($item->obras_presupuestos_id);
    			
     			if($item->delete())
     			{
     			    $itemPadre->precio = $collection->sum('Valor');
    			    $itemPadre->Valor = $itemPadre->precio * $itemPadre->cantidad;
    			
     				return Response::json(array(
     						"Result"			=>		"OK",
     					)
     				);
     			}
     			
    		break;
    		
    	}
    }
    
}
