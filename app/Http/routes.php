<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/home');
});

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});

*/

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/home', 'HomeController@index');

Route::resource('obras', 'ObrasController');

Route::resource('obrasEtapas', 'ObrasEtapasController');

Route::resource('obrasObjetos', 'ObrasObjetosController');

Route::resource('obrasPresupuestos', 'ObrasPresupuestosController');

Route::resource('presupuestoEtapas', 'PresupuestoEtapasController');

Route::resource('presupuestoTipos', 'PresupuestoTiposController');

Route::resource('presupuestoDetalles', 'PresupuestoDetallesController');

Route::resource('presupuestoDetalleTipos', 'PresupuestoDetalleTipoController');

Route::resource('proyectos', 'ProyectosController');

Route::get('proyectos/{objeto_id}/{tipo_id}/{etapa_id?}', 'ProyectosController@objetosPrespuestos')->where('objeto_id', '[0-9]+');

Route::get('proyectos/presupuestodetalle/{id}', 'ProyectosController@presupuestoDetalle');

Route::post('api/proyectos/getData/{action}', 'ProyectosController@getData');

Route::post('api/proyectos/getDataDetalle/{action}', 'ProyectosController@getDataDetalle');

Route::post('api/proyectos/etapaPro', 'ProyectosController@etapaPro');

Route::post('api/proyectos/tipoPro', 'ProyectosController@tipoPro');

/*
Route::get('obrasJson/obrasJson', 'ObrasController@obrasJson');
*/