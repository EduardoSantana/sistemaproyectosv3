<li class="header">MAIN NAVIGATION</li>

<li class="{{ Request::is('proyectos*') ? 'active' : '' }}">
	<a href="{!! route('proyectos.index') !!}"><i class="fa fa-th"></i><span>Proyectos</span></a>
</li>

<!--
<li class="{{ Request::is('obrasPresupuestos*') ? 'active' : '' }}">
	<a href="{!! route('obrasPresupuestos.index') !!}"><i class="fa fa-th"></i><span>ObrasPresupuestos</span></a>
</li>
-->
<!--
<li class="treeview">
	<a href="#">
		<i class="fa fa-th"></i><span>Mantenimientos</span> <i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu">
-->
		<li class="{{ Request::is('obras') ? 'active' : '' }}">
			<a href="{!! route('obras.index') !!}"><i class="fa fa-th"></i><span>Obras</span></a>
		</li>

		<li class="{{ Request::is('obrasEtapas*') ? 'active' : '' }}">
			<a href="{!! route('obrasEtapas.index') !!}"><i class="fa fa-th"></i><span>Etapas</span></a>
		</li>

		<li class="{{ Request::is('obrasObjetos*') ? 'active' : '' }}">
			<a href="{!! route('obrasObjetos.index') !!}"><i class="fa fa-th"></i><span>Objetos de Obra</span></a>
		</li>

		<li class="{{ Request::is('presupuestoEtapas*') ? 'active' : '' }}">
			<a href="{!! route('presupuestoEtapas.index') !!}"><i class="fa fa-th"></i><span>Etapas Constructivas</span></a>
		</li>

		<li class="{{ Request::is('presupuestoTipos*') ? 'active' : '' }}">
			<a href="{!! route('presupuestoTipos.index') !!}"><i class="fa fa-th"></i><span>Tipo de Presupuesto</span></a>
		</li>

		<li class="{{ Request::is('presupuestoDetalleTipos*') ? 'active' : '' }}">
			<a href="{!! route('presupuestoDetalleTipos.index') !!}"><i class="fa fa-th"></i><span>Tipo de Detalle</span></a>
		</li>

	<!--
	</ul>
</li>
-->
