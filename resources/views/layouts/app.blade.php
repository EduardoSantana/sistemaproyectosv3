<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sistema de Proyectos -  Obras de Proyectos</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/AdminLTE.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="{{ url('/Css/site.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

</head>

<body class="skin-red-light sidebar-mini" data-root="{{ url('/') }}">
@if (!Auth::guest())
    <div class="wrapper">
	<!-- Main Header -->
   
	<header class="main-header">
	  
		<!-- Logo -->
		<a href="/" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>S</b>P</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Sistema </b>de Proyectos</span>
		</a>
		
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>

			<!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
			<form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
            </form>
				<ul class="nav navbar-nav">
					
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ url('/images/logo/blue_logo_150x150.jpg') }}"	 class="user-image" alt="User Image">
							<span class="hidden-xs">   
								@if (Auth::guest())
									No Logged
								@else
									{{ Auth::user()->name}}
								@endif
							</span>
						</a>
						<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle" alt="User Image">

							<p>
							@if (Auth::guest())
								No Logged
							@else
								{{ Auth::user()->name }}
							@endif
							<small>Member since Nov. 2012</small>
							</p>
						</li>
						<!-- Menu Body -->
						<li class="user-body">
							<div class="row">
							<div class="col-xs-4 text-center">
								<a href="#">Followers</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Sales</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Friends</a>
							</div>
							</div>
							<!-- /.row -->
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
							<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
							<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
						</ul>
					</li>
					<!-- Control Sidebar Toggle Button 
					<li>
						<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
					</li>
					-->
				</ul>
			</div>
		</nav>
		
	</header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
                <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer 
        <footer class="main-footer" style="max-height: 100px;text-align: center">
           <strong>Copyright © 2016 <a href="#">InfyOm Laravel Generator</a>.</strong> All rights reserved. 
        </footer>
		-->
    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    InfyOm Generator
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@endif
    
    
<!-- Modal -->
<div class="modal fade" id="myModalPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Material 1</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Material 1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Material 2</label>
                        <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Material 2">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail3">Material 3</label>
                        <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Material 3">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail4">Material 4</label>
                        <input type="text" class="form-control" id="exampleInputEmail4" placeholder="Material 4">
                    </div>
                </form>
            </div>
            <!--<div class="modal-footer">-->
            <!--    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            <!--    <button type="button" class="btn btn-primary">Save changes</button>-->
            <!--</div>-->
        </div>
    </div>
</div>
    
	<!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <script src="{{ url('/Content/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

    <!-- AdminLTE App  -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/js/app.min.js"></script>-->
    <script type="text/javascript" src="{{ url('/AdminLTE/app.js') }}"></script>

    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/Scripts/cargarModel.js') }}"></script>

    @yield('scripts')
    

    
    <script type="text/javascript">
        $(document).ready(function() {
          $("select").not(".jtable-left-area select").select2();
        });
        $('.LinkAbajo1').hover(function () {
            $(this).find('.subMenu1').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.subMenu1').stop(true, true).delay(200).fadeOut(500);
        });
    </script>

</body>
</html>