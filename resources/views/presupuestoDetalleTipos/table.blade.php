<table class="table table-responsive" id="presupuestoDetalleTipos-table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($presupuestoDetalleTipos as $presupuestoDetalleTipo)
        <tr>
            <td>{!! $presupuestoDetalleTipo->id !!}</td>
            <td>{!! $presupuestoDetalleTipo->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['presupuestoDetalleTipos.destroy', $presupuestoDetalleTipo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('presupuestoDetalleTipos.show', [$presupuestoDetalleTipo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('presupuestoDetalleTipos.edit', [$presupuestoDetalleTipo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>