<table class="table table-responsive" id="presupuestoTipos-table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($presupuestoTipos as $presupuestoTipos)
        <tr>
            <td>{!! $presupuestoTipos->id !!}</td>
            <td>{!! $presupuestoTipos->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['presupuestoTipos.destroy', $presupuestoTipos->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('presupuestoTipos.show', [$presupuestoTipos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('presupuestoTipos.edit', [$presupuestoTipos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>