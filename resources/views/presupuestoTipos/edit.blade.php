@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            PresupuestoTipos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($presupuestoTipos, ['route' => ['presupuestoTipos.update', $presupuestoTipos->id], 'method' => 'patch']) !!}

                        @include('presupuestoTipos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection