@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            PresupuestoDetalles
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($presupuestoDetalles, ['route' => ['presupuestoDetalles.update', $presupuestoDetalles->id], 'method' => 'patch']) !!}

                        @include('presupuestoDetalles.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection