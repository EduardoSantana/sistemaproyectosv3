<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $presupuestoDetalles->id !!}</p>
</div>

<!-- Obras Presupuestos Id Field -->
<div class="form-group">
    {!! Form::label('obras_presupuestos_id', 'Obras Presupuestos Id:') !!}
    <p>{!! $presupuestoDetalles->obras_presupuestos_id !!}</p>
</div>

<!-- Presupuesto Detalle Tipos Id Field -->
<div class="form-group">
    {!! Form::label('presupuesto_detalle_tipos_id', 'Presupuesto Detalle Tipos Id:') !!}
    <p>{!! $presupuestoDetalles->presupuesto_detalle_tipos_id !!}</p>
</div>

<!-- Codigo Field -->
<div class="form-group">
    {!! Form::label('codigo', 'Codigo:') !!}
    <p>{!! $presupuestoDetalles->codigo !!}</p>
</div>

<!-- Categoria Field -->
<div class="form-group">
    {!! Form::label('categoria', 'Categoria:') !!}
    <p>{!! $presupuestoDetalles->categoria !!}</p>
</div>

<!-- Unidad Field -->
<div class="form-group">
    {!! Form::label('unidad', 'Unidad:') !!}
    <p>{!! $presupuestoDetalles->unidad !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $presupuestoDetalles->descripcion !!}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('precio', 'Precio:') !!}
    <p>{!! $presupuestoDetalles->precio !!}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $presupuestoDetalles->cantidad !!}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('Valor', 'Valor:') !!}
    <p>{!! $presupuestoDetalles->Valor !!}</p>
</div>

