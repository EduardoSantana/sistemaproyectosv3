<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $obrasPresupuestos->id !!}</p>
</div>

<!-- Obra Objeto Id Field -->
<div class="form-group">
    {!! Form::label('obra_objeto_id', 'Obra Objeto Id:') !!}
    <p>{!! $obrasPresupuestos->obra_objeto_id !!}</p>
</div>

<!-- Presupuesto Tipo Id Field -->
<div class="form-group">
    {!! Form::label('presupuesto_tipo_id', 'Presupuesto Tipo Id:') !!}
    <p>{!! $obrasPresupuestos->presupuesto_tipo_id !!}</p>
</div>

<!-- Presupuesto Etapa Id Field -->
<div class="form-group">
    {!! Form::label('presupuesto_etapa_id', 'Presupuesto Etapa Id:') !!}
    <p>{!! $obrasPresupuestos->presupuesto_etapa_id !!}</p>
</div>

<!-- Codigo Field -->
<div class="form-group">
    {!! Form::label('codigo', 'Codigo:') !!}
    <p>{!! $obrasPresupuestos->codigo !!}</p>
</div>

<!-- Categoria Field -->
<div class="form-group">
    {!! Form::label('categoria', 'Categoria:') !!}
    <p>{!! $obrasPresupuestos->categoria !!}</p>
</div>

<!-- Unidad Field -->
<div class="form-group">
    {!! Form::label('unidad', 'Unidad:') !!}
    <p>{!! $obrasPresupuestos->unidad !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $obrasPresupuestos->descripcion !!}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('precio', 'Precio:') !!}
    <p>{!! $obrasPresupuestos->precio !!}</p>
</div>

<!-- Cantidad Field -->
<div class="form-group">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    <p>{!! $obrasPresupuestos->cantidad !!}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('Valor', 'Valor:') !!}
    <p>{!! $obrasPresupuestos->Valor !!}</p>
</div>

