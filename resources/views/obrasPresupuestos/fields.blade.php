<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Obra Objeto Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obra_objeto_id', 'Obra Objeto Id:') !!}
    {!! Form::number('obra_objeto_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Presupuesto Tipo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('presupuesto_tipo_id', 'Presupuesto Tipo Id:') !!}
    {!! Form::number('presupuesto_tipo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Presupuesto Etapa Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('presupuesto_etapa_id', 'Presupuesto Etapa Id:') !!}
    {!! Form::number('presupuesto_etapa_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Codigo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo', 'Codigo:') !!}
    {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
</div>

<!-- Unidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unidad', 'Unidad:') !!}
    {!! Form::text('unidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio', 'Precio:') !!}
    {!! Form::number('precio', null, ['class' => 'form-control']) !!}
</div>

<!-- Cantidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cantidad', 'Cantidad:') !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Valor', 'Valor:') !!}
    {!! Form::number('Valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('obrasPresupuestos.index') !!}" class="btn btn-default">Cancel</a>
</div>
