@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ObrasPresupuestos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($obrasPresupuestos, ['route' => ['obrasPresupuestos.update', $obrasPresupuestos->id], 'method' => 'patch']) !!}

                        @include('obrasPresupuestos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection