<table class="table table-responsive" id="obrasPresupuestos-table">
    <thead>
        <th>Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($obrasPresupuestos as $obrasPresupuestos)
        <tr>
            <td>{!! $obrasPresupuestos->id !!}</td>
            <td>
                {!! Form::open(['route' => ['obrasPresupuestos.destroy', $obrasPresupuestos->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('obrasPresupuestos.show', [$obrasPresupuestos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('obrasPresupuestos.edit', [$obrasPresupuestos->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>