@extends('layouts.app')

@section('content')

<section class="content-header">
	<h1 class="pull-left">
		Proyectos
		<small>
			<span class="label label-primary">{!! $obras->count() !!}</span> - Proyectos de Obras
		</small>
	</h1>
	<h1 class="pull-right">
	 	<a class="btn btn-primary" href="{!! route('obras.create') !!}">Add New</a>
	</h1>
</section>

<div class="clearfix"></div>
<div style="margin-top: 5px;">
	@foreach($obras as $obras)
		<div class="col-md-3 col-sm-6 col-xs-12">
			<a href="{!! route('proyectos.show', [$obras->id]) !!}">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
					<div class="info-box-content">
						<span class="info-box-text"><b>{!! $obras->nombre !!}</b></span>
						<span class="info-box-text">{!! $obras->obrasEtapa->nombre !!}</span>
						<span class="info-box-text">{!! $obras->created_at !!}</span>
					</div>
					<!-- /.info-box-content -->
				</div>
			</a>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
	@endforeach
</div>

@endsection

