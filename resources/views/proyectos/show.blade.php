@extends('layouts.app')

@section('content')

<section class="content-header">
    <h1 class="pull-left">
        {!! $obras->nombre !!}
        <small>
            <span class="label label-primary">{!! $objetosObra->count() !!}</span> - Objetos de Obra
        </small>

    </h1>
    <h1 class="pull-right">
        <a class="btn btn-primary" href="{!! route('obrasObjetos.create') !!}">Add New</a>
    </h1>
</section>
<div class="clearfix"></div>
<div style="margin-top: 5px;">
    @foreach($objetosObra as $objeto)
        <div class="col-md-3 col-sm-6 col-xs-12 ">
            <a href="{{ url('/proyectos/'.$objeto->id.'/1') }}" >
                <div class="info-box LinkAbajo1">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>
                    <div class="info-box-content dropdown-toggle" data-toggle="LinkAbajo1">
                        <span class="info-box-text"><b>{!! $objeto->nombre !!}</b></span>
                        <span class="info-box-text">{!! $objeto->descripcion !!}</span>
                        <span class="info-box-text">{!! $objeto->created_at !!}</span>
                    </div>
                    <ul class="dropdown-menu subMenu1" style="display: none;">
                        <li><a href="#" style=" text-decoration: underline !important;">Tipo de Presupuesto</a></li>
                        @foreach($presupuestoTipo as $item)
                            <li><a href="{{ url('/proyectos/'.$objeto->id.'/'.$item->id ) }}">{!! $item->nombre !!}</a></li>
                        @endforeach
                    </ul>
                    <!-- /.info-box-content -->
                </div>
            </a>
            
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    @endforeach
</div>

@endsection


