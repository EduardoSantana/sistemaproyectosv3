@extends($masterPage)

@section('content')
    <section class="content-header">
        <h1>
            Analisis de Costos de {!! $obraPresupuesto->descripcion !!}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                      <!-- Nav tabs -->
                      <ul id="myTabs" class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" title="Costo Directo de Materiales">Materiales</a></li>
                        <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" title="Costo Directo de Mano de Obra">Mano de Obra</a></li>
                        <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" title="Costo Directo de Equipos">Equipos</a></li>
                        <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" title="Costo Directo de Medios Auxiliares">Medios Auxiliares</a></li>
                      </ul>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tab1">
                            <div id="DetalleMaterialesTableContainer"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab2">
                            <div id="DetalleManoDeObraTableContainer"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab3">
                            <div id="DetalleEquiposTableContainer"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab4">
                            <div id="DetalleAuxiliaresTableContainer"></div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" OnClick="updateDetalle('{!! $presupuesto_id !!}');">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .modal-dialog {
            width: 900px;
        }
    </style>
@endsection

@section('scripts')

    <script>
        var presupuesto_id = "{!! $presupuesto_id !!}";
        var tokenValList1 = "{{csrf_token()}}";
        var tokenValCreate1 = "{{csrf_token()}}";
        var tokenValUpdate1 = "{{csrf_token()}}";
        var tokenValDelete1 = "{{csrf_token()}}";
        var tokenValEtapas1 = "{{csrf_token()}}";
    </script>
    
    <link href="{{ url('/Content/themes/metroblue/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/Scripts/jtable/themes/metro/darkorange/jtable.min.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ url('/Scripts/jquery-ui-1.11.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/jtable/jquery.jtable.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/jtable/localization/jquery.jtable.es.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/number_format.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/proyectos/detalle1Materiales.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/proyectos/detalle2ManoDeObra.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/proyectos/detalle3Equipos.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/proyectos/detalle4Auxiliares.js') }}"></script>
    
@endsection
