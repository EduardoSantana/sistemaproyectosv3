<div id="ProyectosTableContainer"></div>

@section('scripts')

    <script>
        var tituloTabla = "{!! $obrasObjeto->obra->nombre !!} > {!! $obrasObjeto->nombre !!}";
        var objeto_id ="{!! $objeto_id !!}";
        var tipo_id="{!! $tipo_id !!}";
        var etapa_id="{!! $etapa_id !!}";
        var tokenValList = "{{csrf_token()}}";
        var tokenValCreate = "{{csrf_token()}}";
        var tokenValUpdate = "{{csrf_token()}}";
        var tokenValDelete = "{{csrf_token()}}";
        var tokenValEtapas = "{{csrf_token()}}";
    </script>
    
    <!--http://jtable.org/Content/themes/metroblue/jquery-ui.css-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-bootstrap/0.5pre/assets/css/bootstrap.min.css">-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.css">-->
    <!--<link href="{{ url('/Content/themes/custom-theme/jquery-ui-1.10.3.custom.css') }}" rel="stylesheet" type="text/css">-->
    <!--<link href="{{ url('/Scripts/jtable/themes/jqueryui/jtable_jqueryui.css') }}" rel="stylesheet" type="text/css">-->
    <!--<link href="{{ url('/Scripts/jtable/themes/metro/blue/jtable.min.css') }}" rel="stylesheet" type="text/css">-->
    <!--<link href="http://jtable.org/Content/themes/metroblue/jquery-ui.css" rel="stylesheet" type="text/css">-->
    <link href="{{ url('/Content/themes/metroblue/jquery-ui.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('/Scripts/jtable/themes/metro/darkorange/jtable.min.css') }}" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="{{ url('/Scripts/jquery-ui-1.11.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/jtable/jquery.jtable.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/jtable/localization/jquery.jtable.es.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/number_format.js') }}"></script>
    <script type="text/javascript" src="{{ url('/Scripts/proyectos/index.js') }}"></script>
    
@endsection
