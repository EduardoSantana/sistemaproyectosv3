@extends('layouts.app')

@section('content')

<section class="content-header">
    <h1 class="pull-left">
        {!! $obrasObjeto->obra->nombre !!} > {!! $obrasObjeto->nombre !!} > {!! $presupuestoTipo->nombre !!}
    </h1>
</section>

<div class="clearfix"></div>

<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="box box-primary">
        <div class="box-body">
        
            <div class="col-sm-3" style="padding-left: 0px;">
                {!! Form::label('etapa_id','Etapa') !!}
                <select class="form-control" id="etapa_id" name="etapa_id">
                    <option value="0" @if(0 == $etapa_id) selected="selected" @endif>Seleccionar Etapa</option>
                    @forelse($dropDownEtapas as $etapa)
                        <option value="{{$etapa->id}}" 
                            @if($etapa->id == $etapa_id) selected="selected" @endif>
                            {{$etapa->nombre}}
                        </option>
                    @empty
                        <option value="">Seleccionar Etapa</option>
                    @endforelse
                </select>          <div class="clearfix"></div>
            </div>
            <div class="clearfix" style="margin-bottom: 15px;"></div>
            
            @include('proyectos.presupuestosJtable')
            
        </div>
    </div>
</div>

@endsection

