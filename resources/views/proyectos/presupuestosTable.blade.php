<table class="table table-striped table-bordered" cellspacing="0" id="obrasPresupuestos-table">
   <thead>
        <tr>
            <th>Etapa</th>
            <th>Codigo</th>
            <th>Descripcion</th>
            <th>Unid/Med</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Valor</th>
        </tr>
    </thead>
    <tbody>
    @foreach($presupuestoLineas as $obrasPresupuestos)
        <tr>
            <td>{!! $obrasPresupuestos->presupuestoEtapa->nombre !!}</td>
            <td>{!! $obrasPresupuestos->codigo !!}</td>
            <td>{!! $obrasPresupuestos->descripcion !!}</td>
            <td>{!! $obrasPresupuestos->unidad !!}</td>
            <td>
                <!-- Button trigger modal -->
                <button type="button"  data-toggle="modal" data-target="#myModal">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    {!! $obrasPresupuestos->precio !!}
                </button>
            </td>
            <td>{!! $obrasPresupuestos->cantidad !!}</td>
            <td>{!! $obrasPresupuestos->Valor !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Materiales</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Material 1</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Material 1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Material 2</label>
                        <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Material 2">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail3">Material 3</label>
                        <input type="text" class="form-control" id="exampleInputEmail3" placeholder="Material 3">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail4">Material 4</label>
                        <input type="text" class="form-control" id="exampleInputEmail4" placeholder="Material 4">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script>

    $(document).ready(function () {
        $('#obrasPresupuestos-table').DataTable();
    });
</script>
<style>
    #obrasPresupuestos-table_length{
        display:none;
    }
</style>
@endsection
