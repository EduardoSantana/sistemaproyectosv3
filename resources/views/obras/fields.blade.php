<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Obra Etapa Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obra_etapa_id','Etapa:') !!}
    <select class="form-control" id="obra_etapa_id" name="obra_etapa_id">
        @forelse($dropDownObraEtapa as $item)
            <option value="{{$item->id}}" 
                @if(isset($obras) && $item->id == $obras->obra_etapa_id) selected="selected" @endif>
                {{$item->nombre}}
            </option>
        @empty
            <option value="">Seleccionar</option>
        @endforelse
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('obras.index') !!}" class="btn btn-default">Cancel</a>
</div>
