<table class="table table-responsive" id="obras-table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($obras as $obras)
        <tr>
            <td>{!! $obras->id !!}</td>
            <td>{!! $obras->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['obras.destroy', $obras->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('obras.show', [$obras->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('obras.edit', [$obras->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>