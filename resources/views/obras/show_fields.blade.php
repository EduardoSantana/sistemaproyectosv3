<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $obras->id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $obras->nombre !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $obras->descripcion !!}</p>
</div>

<!-- Obra Etapa Id Field -->
<div class="form-group">
    {!! Form::label('obra_etapa_id', 'Obra Etapa Id:') !!}
    <p>{!! $obras->obra_etapa_id !!}</p>
</div>

