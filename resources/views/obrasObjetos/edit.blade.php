@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Objetos de Obra
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($obrasObjetos, ['route' => ['obrasObjetos.update', $obrasObjetos->id], 'method' => 'patch']) !!}

                        @include('obrasObjetos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection