<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $obrasObjetos->id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $obrasObjetos->nombre !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $obrasObjetos->descripcion !!}</p>
</div>

<!-- Obra Id Field -->
<div class="form-group">
    {!! Form::label('obra_id', 'Obra Id:') !!}
    <p>{!! $obrasObjetos->obra_id !!}</p>
</div>

