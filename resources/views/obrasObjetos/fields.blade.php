<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Obra Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('obra_id','Obra:') !!}
    <select class="form-control" id="obra_id" name="obra_id">
        @forelse($dropDownObra as $obra)
            <option value="{{$obra->id}}" 
                @if(isset($obrasObjetos) && $obra->id == $obrasObjetos->obra_id) selected="selected" @endif>
                {{$obra->nombre}}
            </option>
        @empty
            <option value="">Seleccionar</option>
        @endforelse
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('obrasObjetos.index') !!}" class="btn btn-default">Cancel</a>
</div>
