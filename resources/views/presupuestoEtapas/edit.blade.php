@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            PresupuestoEtapas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($presupuestoEtapas, ['route' => ['presupuestoEtapas.update', $presupuestoEtapas->id], 'method' => 'patch']) !!}

                        @include('presupuestoEtapas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection