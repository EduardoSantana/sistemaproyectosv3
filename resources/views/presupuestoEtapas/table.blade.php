<table class="table table-responsive" id="presupuestoEtapas-table">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($presupuestoEtapas as $presupuestoEtapas)
        <tr>
            <td>{!! $presupuestoEtapas->id !!}</td>
            <td>{!! $presupuestoEtapas->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['presupuestoEtapas.destroy', $presupuestoEtapas->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('presupuestoEtapas.show', [$presupuestoEtapas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('presupuestoEtapas.edit', [$presupuestoEtapas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>