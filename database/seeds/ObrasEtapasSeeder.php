<?php

use Illuminate\Database\Seeder;

class ObrasEtapasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('obras_etapas')->insert([
            'nombre' => 'Planeacion',
            'descripcion' => 'Planeacion',
        ]);
		DB::table('obras_etapas')->insert([
            'nombre' => 'Desarrollo',
            'descripcion' => 'Desarrollo',
        ]);
		DB::table('obras_etapas')->insert([
            'nombre' => 'Resultados',
            'descripcion' => 'Resultados',
        ]);
    }
}
