<?php

use Illuminate\Database\Seeder;

class PresupuestoTiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Construccion y Montaje',
            'descripcion' => 'Construccion y Montaje',
        ]);
		DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Facilidades Temporales',
            'descripcion' => 'Facilidades Temporales',
        ]);
		DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Gastos de Trasnportacion',
            'descripcion' => 'Gastos de Trasnportacion',
        ]);
        DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Gastos Adicionales',
            'descripcion' => 'Gastos Adicionales',
        ]);
        DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Imprevistos Tecnicos',
            'descripcion' => 'Imprevistos Tecnicos',
        ]);
        DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Imprevistos Economicos',
            'descripcion' => 'Imprevistos Economicos',
        ]);
        DB::table('presupuesto_tipos')->insert([
            'nombre' => 'Bancario',
            'descripcion' => 'Bancario',
        ]);
         DB::table('presupuesto_tipos')->insert([
            'nombre' => 'De Seguros',
            'descripcion' => 'De Seguros',
        ]);
    }
}
