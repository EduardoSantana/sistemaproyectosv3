<?php

use Illuminate\Database\Seeder;

class ObrasObjetosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('obras_objetos')->insert([
            'nombre' => 'Construccion General',
            'descripcion' => 'Construccion General',
        ]);
    }
}
