<?php

use Illuminate\Database\Seeder;

class PresupuestoDetalleTiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('presupuesto_detalle_tipos')->insert([
            'nombre' => 'Costo Directo de Materiales',
            'descripcion' => 'Costo Directo de Materiales',
        ]);
		DB::table('presupuesto_detalle_tipos')->insert([
            'nombre' => 'Costo Directo de Mano de Obra',
            'descripcion' => 'Costo Directo de Mano de Obra',
        ]);
		DB::table('presupuesto_detalle_tipos')->insert([
            'nombre' => 'Costo Directo de Equipos',
            'descripcion' => 'Costo Directo de Equipos',
        ]);
		DB::table('presupuesto_detalle_tipos')->insert([
            'nombre' => 'Costo Directo de Medios Auxiliares',
            'descripcion' => 'Costo Directo de Medios Auxiliares',
        ]);
    }
}
