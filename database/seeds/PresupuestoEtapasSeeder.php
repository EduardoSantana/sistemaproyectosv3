<?php

use Illuminate\Database\Seeder;

class PresupuestoEtapasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('presupuesto_etapas')->insert([
            'nombre' => 'Movimiento de Tierra',
            'descripcion' => 'Movimiento de Tierra',
        ]);
		DB::table('presupuesto_etapas')->insert([
            'nombre' => 'Cimentaciones',
            'descripcion' => 'Cimentaciones',
        ]);
		DB::table('presupuesto_etapas')->insert([
            'nombre' => 'Instalaciones Electricas ',
            'descripcion' => 'Instalaciones Electricas',
        ]);
    }
}
