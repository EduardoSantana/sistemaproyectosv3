<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
		$this->call(ObrasEtapasSeeder::class);
		$this->call(ObrasObjetosSeeder::class);
		$this->call(PresupuestoDetalleTiposSeeder::class);
		$this->call(PresupuestoEtapasSeeder::class);
        $this->call(PresupuestoTiposSeeder::class);
    }
}
