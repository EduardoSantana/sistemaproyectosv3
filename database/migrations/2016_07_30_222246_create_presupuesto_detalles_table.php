<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresupuestoDetallesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuesto_detalles', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('obras_presupuestos_id');
            $table->integer('presupuesto_detalle_tipos_id');
            $table->string('codigo', 255);
            $table->string('categoria', 255);
            $table->string('unidad', 20);
            $table->string('descripcion', 255);
            $table->decimal('precio');
            $table->decimal('cantidad');
            $table->decimal('Valor');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('presupuesto_detalles');
    }
}
