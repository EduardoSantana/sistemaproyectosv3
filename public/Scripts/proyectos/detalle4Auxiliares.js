$(document).ready(function () 
{
    var presupuesto_detalle_tipos_id4 = 4;
    var  nombreDeTabla4 = '#DetalleAuxiliaresTableContainer';
    //Prepare jTable
    $( nombreDeTabla4).jtable({
        title: 'Costos Directos de Axuliares',
        paging: true,
        sorting: true,
        actions: {
            listAction: '/api/proyectos/getDataDetalle/list?_token=' + tokenValList1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id4,
            createAction: '/api/proyectos/getDataDetalle/create?_token=' + tokenValCreate1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id4 + '&presupuesto_id=' + presupuesto_id,
            updateAction: '/api/proyectos/getDataDetalle/update?_token=' + tokenValUpdate1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id4 + '&presupuesto_id=' + presupuesto_id,
            deleteAction: '/api/proyectos/getDataDetalle/delete?_token=' + tokenValUpdate1
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            codigo: {
                title: 'Codigo'
            },
            descripcion: {
                title: 'Descripcion'
            },
            unidad: {
                title: 'Unidad'
            },
            precio: {
                title: 'Precio'
            },
            cantidad: {
                title: 'Cantidad'
            },
            Valor: {
                title: 'Valor',
                create: false,
                edit: false
            }
        }
    });
                        
    //Load person list from server
    //$( nombreDeTabla4).jtable('load');
    //'&tipo_id=' + tipo_id + '&etapa_id=' + etapa_id, 
    
    $( nombreDeTabla4).jtable('load', {
        presupuesto_id: presupuesto_id
    });
  

});