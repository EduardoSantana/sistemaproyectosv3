function popUpDetalle(id){
    var urlPath = '/proyectos/presupuestodetalle/' + id;
    cargarModal(urlPath);
}

function updateDetalle(id){
    // var urlPath = '/proyectos/presupuestodetalle/' + id;
    // cargarModal(urlPath);
    $('#ProyectosTableContainer').jtable('updateRecord', {
        record: {
            id: id,
            precio: -1
        }
    });
}

$(document).ready(function () 
{
    var nombreDeTabla = '#ProyectosTableContainer';
    //Prepare jTable
    $(nombreDeTabla).jtable({
        title: tituloTabla,
        paging: true,
        sorting: true,
        actions: {
            listAction: '/api/proyectos/getData/list?_token=' + tokenValList + '&objeto_id=' + objeto_id,
            createAction: '/api/proyectos/getData/create?_token=' + tokenValCreate + '&objeto_id=' + objeto_id + '&tipo_id=' + tipo_id,
            updateAction: '/api/proyectos/getData/update?_token=' + tokenValUpdate + '&objeto_id=' + objeto_id + '&tipo_id=' + tipo_id,
            deleteAction: '/api/proyectos/getData/delete?_token=' + tokenValDelete + '&objeto_id=' + objeto_id + '&tipo_id=' + tipo_id
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            presupuesto_tipo_id: {
                title: 'Tipo',
                edit: false,
                list: false,
                options: '/api/proyectos/tipoPro?_token=' + tokenValEtapas
            },
            presupuesto_etapa_id: {
                title: 'Etapa',
                options: '/api/proyectos/etapaPro?_token=' + tokenValEtapas,
                width: '5%'
            },
            codigo: {
                title: 'Codigo',
                width: '5%'
            },
            descripcion: {
                title: 'Descripcion',
                width: '20%'
            },
            unidad: {
                title: 'Unidad',
                width: '5%'
            },
            precio: {
                title: 'Precio',
                display: function (data) {
                    return '<a  class="" OnClick="popUpDetalle(' + "'" + data.record.id + "'" +');"> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </a> '  + data.record.precio ; 
                },
                width: '5%'
            },
            cantidad: {
                title: 'Cantidad',
                width: '5%'
            },
            Valor: {
                title: 'Valor',
                create: false,
                edit: false,
                width: '5%'
            }
        }
    });
                        
    $(nombreDeTabla).jtable('load', {
        tipo_id: tipo_id,
        etapa_id: $('#etapa_id').val()
    });
   
    $( '#etapa_id' ).change(function() {
       $(nombreDeTabla).jtable('load', {
            tipo_id: tipo_id,
            etapa_id: $('#etapa_id').val()
        });
    });

});