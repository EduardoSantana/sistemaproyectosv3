$(document).ready(function () 
{
    var presupuesto_detalle_tipos_id3 = 3;
    var  nombreDeTabla3 = '#DetalleEquiposTableContainer';
    //Prepare jTable
    $( nombreDeTabla3).jtable({
        title: 'Costos Directos de Equipos',
        paging: true,
        sorting: true,
        actions: {
            listAction: '/api/proyectos/getDataDetalle/list?_token=' + tokenValList1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id3,
            createAction: '/api/proyectos/getDataDetalle/create?_token=' + tokenValCreate1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id3 + '&presupuesto_id=' + presupuesto_id,
            updateAction: '/api/proyectos/getDataDetalle/update?_token=' + tokenValUpdate1 + '&presupuesto_detalle_tipos_id=' + presupuesto_detalle_tipos_id3 + '&presupuesto_id=' + presupuesto_id,
            deleteAction: '/api/proyectos/getDataDetalle/delete?_token=' + tokenValUpdate1
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            codigo: {
                title: 'Codigo'
            },
            descripcion: {
                title: 'Descripcion'
            },
            unidad: {
                title: 'Unidad'
            },
            precio: {
                title: 'Precio'
            },
            cantidad: {
                title: 'Cantidad'
            },
            Valor: {
                title: 'Valor',
                create: false,
                edit: false
            }
        }
    });
                        
    //Load person list from server
    //$( nombreDeTabla3).jtable('load');
    //'&tipo_id=' + tipo_id + '&etapa_id=' + etapa_id, 
    
    $( nombreDeTabla3).jtable('load', {
        presupuesto_id: presupuesto_id
    });
  

});